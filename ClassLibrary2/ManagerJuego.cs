﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clases
{
    public class ManagerJuego
    {
        public Mazo mazoCartas { get; set; }
        public ManagerJuego()
        {
            Mazo m = new Mazo(); //LAS CLASES SE TIENEN QUE INSTANCIAR COMO UN OBJETO. (M ES UN OBJETO MAZO)
            m.Generar();
            //mazoCartas = m; //SACO EL MAZO DE CARTAS, LO PUSO ENCIMA DE LA MESA
        }
    }
}
