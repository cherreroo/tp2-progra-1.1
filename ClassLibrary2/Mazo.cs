﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clases
{
    public class Mazo
    {
        public List<Carta> cartas { get; set; }

        public Mazo()
        {
            cartas = new List<Carta>();
        }
        public void Mezclar() //CON UN VALOR AL AZAR, VOY INTERCAMBIANDO LAS POSICIONES
        {  //PASO A LA 1, LA CAMBIO DE POSICION, PASO A LA 2, CAMBIO... ETC
            Random rand = new Random(); //EMPEZANDO DE LA ULTIMA HASTA LA PRIMERA (PARA QUE NO ME PREOCUPE LA CANTIDAD)
            int posUltimaCarta = cartas.Count; //EL VALOR DE COUNT ES IGUAL A LA ULTIMA CARTA.

            while (posUltimaCarta > 1)
            {
                posUltimaCarta--;
                int posCartaParaCambiar = rand.Next(posUltimaCarta + 1); //NUMERO AL AZAR ENTRE 0 Y 51 ACTUALMENTE
                Carta cartaTemp = cartas[posCartaParaCambiar];
                cartas[posCartaParaCambiar] = cartas[posUltimaCarta];
                cartas[posUltimaCarta] = cartaTemp; //TOMA LA POSICION QUE GENERE ANTERIORMENTE
            }

        }
        public void Generar() //RECORDAR QUE HAY QUE GENERAR EL MAZO CUANDO ARRANCO
        {
            int cont = 0; //UN CONTADOR USADO PARA LOCALIZAR LA IMAGEN

            //TENGO QUE GENERAR UN MAZO DE CARTAS COMPLETO
            Carta carta; //GENERANDO UN OBJETO DE TIPO CARTA

            for (int numPalo = 0; numPalo < 4; numPalo++) //RECORRER Y GENERAR LOS PALOS/TIPOS
            {
                for (int numCarta = 1; numCarta <= 13; numCarta++)//RECORRER Y GENERAR LAS 13 CARTAS (PARA EL NUMERO 0, GENERAME LAS 13 CARTAS.. ETC)
                {
                    carta = new Carta(); //EL NUMERO DE CARTA VA A SER EL QUE LE TOQUE DEL FOR
                    carta.numero = numCarta;

                    //TRANSFORMAR EL NUMERO DE PALO AL STRING QUE LE CORRESPONDA AL NUMERO DE CARTA
                    if (numPalo == 0) { carta.palo = Carta.enumPalo.pica; }
                    else if (numPalo == 1) { carta.palo = Carta.enumPalo.diamante; }
                    else if (numPalo == 2) { carta.palo = Carta.enumPalo.trebol; }
                    else if (numPalo == 3) { carta.palo = Carta.enumPalo.corazon; }

                    //NUMERO DE CARTA, CHEQUEAR (TE ESTARIA DANDO LA POSICION DE LA IMAGEN EN LA PILA DE CARTAS)
                    //carta.imagen = numCarta + (12 + (numPalo - 1));
                    carta.imagen = cont; //LE DOY EL NUMERO DE POSICION PARA QUE DE LA LISTA DE IMAGENES ELIJA UNA
                    cartas.Add(carta);
                    cont += 1;
                }
            }
        }
    }
}
