﻿using Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TEST
{
    public partial class FrmJuego : Form
    {
        Mazo mazo;
        List<Image> imglistMazo = new List<Image>();


        public FrmJuego()
        {
            InitializeComponent();
        }

        private void frmJuego_Load(object sender, EventArgs e)
        {
            //Esconder botones innecesarios
            btnReiniciar.Hide();
            btnAgarrar.Show();

            //Hacer mazo y añadir las imagenes
            mazo = new Mazo();
            mazo.Generar();
            imglistMazo.Add(Properties.Resources._1_corazon);
            imglistMazo.Add(Properties.Resources._1_diamante);
            imglistMazo.Add(Properties.Resources._1_pica);
            imglistMazo.Add(Properties.Resources._1_trebol);
            imglistMazo.Add(Properties.Resources._2_corazon);
            imglistMazo.Add(Properties.Resources._2_diamante);
            imglistMazo.Add(Properties.Resources._2_pica);
            imglistMazo.Add(Properties.Resources._2_trebol);
            imglistMazo.Add(Properties.Resources._3_corazon);
            imglistMazo.Add(Properties.Resources._3_diamante);
            imglistMazo.Add(Properties.Resources._3_pica);
            imglistMazo.Add(Properties.Resources._3_trebol);
            imglistMazo.Add(Properties.Resources._4_corazon);
            imglistMazo.Add(Properties.Resources._4_diamante);
            imglistMazo.Add(Properties.Resources._4_pica);
            imglistMazo.Add(Properties.Resources._4_trebol);
            imglistMazo.Add(Properties.Resources._5_corazon);
            imglistMazo.Add(Properties.Resources._5_diamante);
            imglistMazo.Add(Properties.Resources._5_pica);
            imglistMazo.Add(Properties.Resources._5_trebol);
            imglistMazo.Add(Properties.Resources._6_corazon);
            imglistMazo.Add(Properties.Resources._6_diamante);
            imglistMazo.Add(Properties.Resources._6_pica);
            imglistMazo.Add(Properties.Resources._6_trebol);
            imglistMazo.Add(Properties.Resources._7_corazon);
            imglistMazo.Add(Properties.Resources._7_diamante);
            imglistMazo.Add(Properties.Resources._7_pica);
            imglistMazo.Add(Properties.Resources._7_trebol);
            imglistMazo.Add(Properties.Resources._8_corazon);
            imglistMazo.Add(Properties.Resources._8_diamante);
            imglistMazo.Add(Properties.Resources._8_pica);
            imglistMazo.Add(Properties.Resources._8_trebol);
            imglistMazo.Add(Properties.Resources._9_corazon);
            imglistMazo.Add(Properties.Resources._9_diamante);
            imglistMazo.Add(Properties.Resources._9_pica);
            imglistMazo.Add(Properties.Resources._9_trebol);
            imglistMazo.Add(Properties.Resources._10_corazon);
            imglistMazo.Add(Properties.Resources._10_diamante);
            imglistMazo.Add(Properties.Resources._10_pica);
            imglistMazo.Add(Properties.Resources._10_trebol);
            imglistMazo.Add(Properties.Resources.J_corazon);
            imglistMazo.Add(Properties.Resources.J_diamante);
            imglistMazo.Add(Properties.Resources.J_pica);
            imglistMazo.Add(Properties.Resources.J_trebol);
            imglistMazo.Add(Properties.Resources.Q_corazon);
            imglistMazo.Add(Properties.Resources.Q_diamante);
            imglistMazo.Add(Properties.Resources.Q_pica);
            imglistMazo.Add(Properties.Resources.Q_trebol);
            imglistMazo.Add(Properties.Resources.K_corazon);
            imglistMazo.Add(Properties.Resources.K_diamante);
            imglistMazo.Add(Properties.Resources.K_pica);
            imglistMazo.Add(Properties.Resources.K_trebol);

        }

        private void btnAgarrar_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int cont = 0;

            //for (int i = 0; i < mazo.cartas.Count; i++)
            //{
            if (cont < mazo.cartas.Count)
            {
                int i = 0;

                i = random.Next(0, mazo.cartas.Count);

                if (mazo.cartas[i].yaSalio == false)
                {   
                    pcbJugador.Image = imglistMazo[i];
                    pcbJugador.SizeMode = PictureBoxSizeMode.StretchImage;
                    mazo.cartas[i].yaSalio = true;

                    if (mazo.cartas[i].numero == 2 && mazo.cartas[i].palo == Carta.enumPalo.pica) //Con el 1 de diamante
                    {
                        MessageBox.Show("¡Perdiste!", "Termino", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        ReiniciarJuego();
                        return;
                    }
                }

                i = random.Next(0, mazo.cartas.Count);

                if (mazo.cartas[i].yaSalio == false)
                {
                    pcbPC.Image = imglistMazo[i];
                    pcbPC.SizeMode = PictureBoxSizeMode.StretchImage;
                    mazo.cartas[i].yaSalio = true;
                    if (mazo.cartas[i].numero == 2 && mazo.cartas[i].palo == Carta.enumPalo.pica)
                    {
                        MessageBox.Show("¡Ganaste!", "Termino", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ReiniciarJuego();
                        return;
                    }
                }

                i++;
                cont++;
            }
            void ReiniciarJuego()
            {
                btnAgarrar.Hide();
                btnReiniciar.Show();
            }
            //}
        }

        private void pcbJugador_Click(object sender, EventArgs e)
        {

        }

        private void btnReiniciar_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < mazo.cartas.Count; i++)
            {
                mazo.cartas[i].yaSalio = false;
            }
            mazo = new Mazo();
            mazo.Generar();
            btnAgarrar.Show();
            btnReiniciar.Hide();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

