﻿using System.Drawing;

namespace TEST
{
    partial class FrmJuego
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmJuego));
            this.lblPC = new System.Windows.Forms.Label();
            this.lblJugador = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnReiniciar = new System.Windows.Forms.Button();
            this.pcbJugador = new System.Windows.Forms.PictureBox();
            this.pcbPC = new System.Windows.Forms.PictureBox();
            this.pcbMazo = new System.Windows.Forms.PictureBox();
            this.btnAgarrar = new System.Windows.Forms.Button();
            this.imgJugador = new System.Windows.Forms.PictureBox();
            this.imgPC = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pcbJugador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMazo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgJugador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPC)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPC
            // 
            this.lblPC.AutoSize = true;
            this.lblPC.Location = new System.Drawing.Point(97, 196);
            this.lblPC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPC.Name = "lblPC";
            this.lblPC.Size = new System.Drawing.Size(0, 13);
            this.lblPC.TabIndex = 4;
            // 
            // lblJugador
            // 
            this.lblJugador.AutoSize = true;
            this.lblJugador.Location = new System.Drawing.Point(445, 208);
            this.lblJugador.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblJugador.Name = "lblJugador";
            this.lblJugador.Size = new System.Drawing.Size(0, 13);
            this.lblJugador.TabIndex = 5;
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.Red;
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(346, 231);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 25);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnReiniciar
            // 
            this.btnReiniciar.BackColor = System.Drawing.Color.Red;
            this.btnReiniciar.Image = ((System.Drawing.Image)(resources.GetObject("btnReiniciar.Image")));
            this.btnReiniciar.Location = new System.Drawing.Point(241, 206);
            this.btnReiniciar.Margin = new System.Windows.Forms.Padding(2);
            this.btnReiniciar.Name = "btnReiniciar";
            this.btnReiniciar.Size = new System.Drawing.Size(100, 50);
            this.btnReiniciar.TabIndex = 6;
            this.btnReiniciar.UseVisualStyleBackColor = false;
            this.btnReiniciar.Click += new System.EventHandler(this.btnReiniciar_Click);
            // 
            // pcbJugador
            // 
            this.pcbJugador.Image = global::TEST.Properties.Resources._1_diamante;
            this.pcbJugador.Location = new System.Drawing.Point(420, 52);
            this.pcbJugador.Margin = new System.Windows.Forms.Padding(2);
            this.pcbJugador.Name = "pcbJugador";
            this.pcbJugador.Size = new System.Drawing.Size(95, 123);
            this.pcbJugador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbJugador.TabIndex = 3;
            this.pcbJugador.TabStop = false;
            this.pcbJugador.Click += new System.EventHandler(this.pcbJugador_Click);
            // 
            // pcbPC
            // 
            this.pcbPC.Image = global::TEST.Properties.Resources._1_diamante;
            this.pcbPC.Location = new System.Drawing.Point(62, 52);
            this.pcbPC.Margin = new System.Windows.Forms.Padding(2);
            this.pcbPC.Name = "pcbPC";
            this.pcbPC.Size = new System.Drawing.Size(95, 123);
            this.pcbPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbPC.TabIndex = 2;
            this.pcbPC.TabStop = false;
            // 
            // pcbMazo
            // 
            this.pcbMazo.BackColor = System.Drawing.Color.Transparent;
            this.pcbMazo.Image = global::TEST.Properties.Resources.dorso;
            this.pcbMazo.Location = new System.Drawing.Point(210, 37);
            this.pcbMazo.Margin = new System.Windows.Forms.Padding(2);
            this.pcbMazo.Name = "pcbMazo";
            this.pcbMazo.Size = new System.Drawing.Size(159, 138);
            this.pcbMazo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbMazo.TabIndex = 1;
            this.pcbMazo.TabStop = false;
            // 
            // btnAgarrar
            // 
            this.btnAgarrar.BackColor = System.Drawing.Color.Red;
            this.btnAgarrar.Image = global::TEST.Properties.Resources.Frame_266;
            this.btnAgarrar.Location = new System.Drawing.Point(241, 206);
            this.btnAgarrar.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgarrar.Name = "btnAgarrar";
            this.btnAgarrar.Size = new System.Drawing.Size(100, 50);
            this.btnAgarrar.TabIndex = 0;
            this.btnAgarrar.UseVisualStyleBackColor = false;
            this.btnAgarrar.Click += new System.EventHandler(this.btnAgarrar_Click);
            // 
            // imgJugador
            // 
            this.imgJugador.Image = ((System.Drawing.Image)(resources.GetObject("imgJugador.Image")));
            this.imgJugador.InitialImage = ((System.Drawing.Image)(resources.GetObject("imgJugador.InitialImage")));
            this.imgJugador.Location = new System.Drawing.Point(420, 180);
            this.imgJugador.Name = "imgJugador";
            this.imgJugador.Size = new System.Drawing.Size(95, 25);
            this.imgJugador.TabIndex = 8;
            this.imgJugador.TabStop = false;
            // 
            // imgPC
            // 
            this.imgPC.Image = ((System.Drawing.Image)(resources.GetObject("imgPC.Image")));
            this.imgPC.InitialImage = ((System.Drawing.Image)(resources.GetObject("imgPC.InitialImage")));
            this.imgPC.Location = new System.Drawing.Point(62, 180);
            this.imgPC.Name = "imgPC";
            this.imgPC.Size = new System.Drawing.Size(95, 25);
            this.imgPC.TabIndex = 9;
            this.imgPC.TabStop = false;
            // 
            // FrmJuego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(584, 261);
            this.Controls.Add(this.imgPC);
            this.Controls.Add(this.imgJugador);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnReiniciar);
            this.Controls.Add(this.lblJugador);
            this.Controls.Add(this.lblPC);
            this.Controls.Add(this.pcbJugador);
            this.Controls.Add(this.pcbPC);
            this.Controls.Add(this.pcbMazo);
            this.Controls.Add(this.btnAgarrar);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmJuego";
            this.Text = "As Mortal";
            this.Load += new System.EventHandler(this.frmJuego_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcbJugador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMazo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgJugador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAgarrar;
        private System.Windows.Forms.PictureBox pcbMazo;
        private System.Windows.Forms.PictureBox pcbPC;
        private System.Windows.Forms.PictureBox pcbJugador;
        private System.Windows.Forms.Label lblPC;
        private System.Windows.Forms.Label lblJugador;
        private System.Windows.Forms.Button btnReiniciar;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.PictureBox imgJugador;
        private System.Windows.Forms.PictureBox imgPC;
    }
}

